import { useState, useEffect } from "react";
import axios from "axios";
import { sendSubscribe } from "./subscribeFetch";
import { validateEmail } from "./email-validator";
import { unsubscribeUser } from "./unsubscribeFetch";

const JoinUsSection = (props) => {
  // const { placeholder } = props;
  const [email, setEmail] = useState(() => {
    const localEmail = localStorage.getItem("Email");
    return localEmail !== null ? localEmail : "";
  });

  const [isDisabled, setDisabled] = useState(false);
  const [isSubscribed, setIsSubscribed] = useState(false);
  const [displayMode, setDisplayMode] = useState("block");
  const [opacity, setOpacity] = useState(1);

  useEffect(() => {
    const buttonState = localStorage.getItem("Button state");
    if (buttonState) {
      setIsSubscribed(JSON.parse(buttonState));
    }
  }, []);

  useEffect(() => {
    const displayMode = localStorage.getItem("displayMode");
    if (displayMode) {
      setDisplayMode(JSON.parse(displayMode));
    }
  }, []);

  const handleSubscribe = async () => {
    if (!validateEmail(email)) {
      console.log("Invalid email address");
      alert("Invalid email address. Please try again.");
      return;
    }

    try {
      const response = await axios.post("http://localhost:3000/subscribe", {
        email: email,
      });

      if (response.status === 200) {
        console.log("Subscription successful!", response.data);
        alert("Subscription successful!");
        setIsSubscribed(true);
        localStorage.setItem("Button state", "true");
      } else if (response.status === 422) {
        const errorResponse = await response.json();
        console.error("Error subscribing:", errorResponse.error);
        alert("Error subscribing: " + errorResponse.error);
      } else {
        console.error("Error subscribing:", response.statusText);
        alert("Error subscribing: " + response.statusText);
      }
    } catch (error) {
      console.error("Error subscribing:", error);
      alert("Error subscribing: " + error.message);
    }
  };

  const unsubscribeEmail = async () => {
    try {
      const response = await axios.post("http://localhost:3000/unsubscribe", {
        email: email,
      });

      if (response.data.success) {
        setIsSubscribed(false);
        setDisplayMode("block");
        localStorage.setItem("Button state", "false");
        setEmail("");
        localStorage.removeItem("Email");
        alert("Unsubscribed successfully!");
      } else {
        alert("Error unsubscribing: " + response.data.message);
      }
    } catch (error) {
      console.error("Error unsubscribing:", error);
      alert("Error unsubscribing: " + error.message);
    }
  };

  const changeButtonState = () => {
    if (isSubscribed) {
      unsubscribeEmail();
    } else {
      handleSubscribe();
    }
  };

  const disableBtn = () => {
    setOpacity(0.5);
    setDisabled(true);
  };

  const enableBtn = () => {
    setOpacity(1);
    setDisabled(false);
  };

  return (
    <>
      <main id="app-container">
        <section
          className="app-section app-section--image-program"
          id="programContainer"
        >
          <h2 className="program-title">Join Our Program</h2>
          <h3 className="program-subtitle">
            Sed do eiusmod tempor incididunt
            <br />
            ut labore et dolore magna aliqua
          </h3>
          <form className="submitFieldWrapper" id="form">
            <div
              className="form-wrapper"
              id="emailForm"
              style={{
                display: displayMode,
              }}
            >
              <input
                className="form-input"
                id="submit-info"
                type="text"
                placeholder="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <input
              id="subscribeButton"
              className="app-section__button submit-btn"
              type="button"
              value={isSubscribed ? "Unsubscribe" : "Subscribe"}
              style={{
                opacity,
              }}
              onClick={() => {
                if (!isDisabled) {
                  disableBtn();
                  changeButtonState();
                  setTimeout(enableBtn, 2000);
                }
              }}
              disabled={isDisabled}
            />
          </form>
        </section>
      </main>
    </>
  );
};

export default JoinUsSection;
